var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development',
    port = process.env.PORT || 80,
    productionDb = process.env.MONGOLAB_URI;

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'mentormatch'
    },
    port: port,
    db: 'mongodb://localhost/mentormatch-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'mentormatch'
    },
    port: port,
    db: 'mongodb://localhost/mentormatch-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'mentormatch'
    },
    port: port,
    db: productionDb
  }
};

module.exports = config[env];
