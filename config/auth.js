var mongoose = require('mongoose'),
    FacebookStrategy = require('passport-facebook').Strategy,
    LinkedInStrategy = require('passport-linkedin').Strategy,
    fbHelper = require('../app/helpers/facebook'),
    env = process.env.NODE_ENV || 'development'
    linkedinHelper = require('../app/helpers/linkedin');

var User = mongoose.model('User');

module.exports = function(passport) {

  // Username and password auth
  passport.use(User.createStrategy());
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  // Facebook auth
  passport.use('facebook', new FacebookStrategy({
      clientID: "288627761509354",
      clientSecret: "a5e12977addfc7d0e0c896bc5a1be266",
      callbackURL: env === 'development' ? "http://mentordemo.herokuapp.dev/facebook/callback" : "https://mentordemo.herokuapp.com/facebook/callback",
      profileFields: ['id','displayName','email','likes','picture.type(large)'],
      passReqToCallback: true
    },
    function(req, accessToken, refreshToken, profile, done) {

      var fbData = {
        profile: profile, accessToken: accessToken
      };

      if (req.user) { // User logged in, link FB to account
        fbHelper.linkAccount(req.user, fbData, done);
      } else { // User not logged in, try logging in or registering
        var email = profile.emails[0].value;
        User.findOne({ email: email }, function(err, user) {
          if (err) { return done(err); }
          else if (!user) fbHelper.registerWithFacebook(fbData, done);
          else fbHelper.linkAccount(user, fbData, done);
        });
      }
    }
  ));

  // LinkedIn auth
  passport.use('linkedin', new LinkedInStrategy({
    consumerKey: "77ualwr8w8zwhg",
    consumerSecret: "CtajMmyXZNygvQQ2",
    callbackURL: "https://mentordemo.herokuapp.com/linkedin/callback",
    profileFields: ['id', 'first-name', 'last-name', 'email-address', 'site-standard-profile-request'],
    passReqToCallback: true
  },
  function(req, token, tokenSecret, profile, done) {

    if (req.user) { // User logged in, link account
      linkedinHelper.linkAccount(req.user, profile, done);
    } else { // User not logged in, find or register
      var email = profile.emails[0].value;
      User.findOne({ email: email }, function (err, user) {
        if (err) { return done(err); }
        else if (!user) linkedinHelper.register(profile, done);
        else linkedinHelper.linkAccount(user, profile, done);
      });
    }
  }));

}
