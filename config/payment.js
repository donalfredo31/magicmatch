var SecureNet = require('securenet-js');

var configSecureNet = {
    credentials: {
        secureNetId: '8007214', //provided via signup email 
        secureNetKey: '1mKaP3lwc2dF' //provided inside virtual terminal 
    },
    developerApplication: {
        developerId: 12345678, // provided by SecureNet after certification 
        version: '1.2'
    },
    mode: 'test' //live or test 
};

module.exports =  function(){
	return new SecureNet(configSecureNet);
}