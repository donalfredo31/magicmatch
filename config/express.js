var express = require('express');
var glob = require('glob');

var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');


var session = require('express-session');
var MongoStore = require('connect-mongo')(session);


var dbTests = require('../app/helpers/db_tests');
var securenet = require('../app/helpers/securenet');


module.exports = function(app, passport, mongoose, config) {

  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';

  
  dbTests.runTests();
  securenet.init();

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public'));
  app.use(methodOverride());

  // Sessions
  app.use(session({
    secret: 'men&tor%list',
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
  }));

  // Additional middleware
   


  // Authentication
  app.use(passport.initialize());
  app.use(passport.session());



  //https redirect for heroku
  if(app.get('env') === 'production'){
    app.use(function(req, res, next){
      
      if (req.headers['x-forwarded-proto'] !== 'https') 
        return res.redirect(['https://', req.get('Host'), req.url].join(''));
      
      return next();

    });
  }


  // Add controllers to express app
  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });



  // If no route matches, create a 404 error
  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // Final resort error renderers
  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.send({
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  } else {
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
        res.send({
          message: err.message,
          error: {},
          title: 'error'
        });
    });
  }


};
