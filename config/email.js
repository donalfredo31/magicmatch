var smtpTransport = require('nodemailer-smtp-transport');

var smtpConfig = {
	host : 'smtp.gmail.com',
	port : '587',
	secureConnection : true,
	auth : {
		user : 'notifications@collegeconnections.co.uk',
		pass : 'mentormatch'
	}
};

module.exports = function(nodeMailer){

	var transport = nodeMailer.createTransport((smtpTransport(smtpConfig)));
	return transport;

}