var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose'),
    Conversation = mongoose.model('Conversation', Conversation);

var Comment = new Schema({
  text: 'string',
  from: { type: Schema.Types.ObjectId, ref: 'User' },
  timestamp: 'date'
});
mongoose.model('Comment', Comment);


//references of to and from to be able to find specific feedbacks
var Feedback = new Schema({
  to : { type: Schema.Types.ObjectId, ref: 'User' },
  from : { type: Schema.Types.ObjectId, ref: 'User' },
  timestamp: 'date',
  questions : [{
    answer : 'string',
    rating : 'number',
    question : 'string'
  }]
});
mongoose.model('Feedback', Feedback);

var Analysis = new Schema({
  openness: 'number',
  neuroticism: 'number',
  agreeableness: 'number',
  extraversion: 'number',
  conscientiousness: 'number'
});

var LinkedIn = new Schema({
  id: 'string',
  url: 'string'
});

function validateRoles (roles) {
  var rolesPresent = roles && roles.length > 0;
  var mentor = roles.indexOf('mentor') != -1;
  var learner = roles.indexOf('learner') != -1;
  return rolesPresent && (mentor || learner);
}

var User = new Schema({

  //verfication
  emailVerification : {
      isVerified : 'bool',
      token : 'string',
      expiration : 'date'
  },
  passwordReset : {
    token : 'string',
    expiration : 'date'
  },

  isApproved : 'bool',

  //call credit
  credit : 'number', //in seconds

  //profile
  secureNet: {
    customerId : 'string',
    paymentMethodIds : ['object']
  },
  roles: ['string'],
  fullName: 'string',
  avatar : 'string',
  aboutme : 'string',
  subjects : ['string'],
  fieldOfStudy : ['string'],
  skype: 'string',
  facebook: 'string',
  calendly: 'string',
  likes: ['string'],
  linkedin: LinkedIn,
  analysis: Analysis,
  conversations : [{type: Schema.Types.ObjectId, ref: 'Conversation'}],
  comments: [{type: Schema.Types.ObjectId, ref: 'Comment'}],
  feedbacks : [{type: Schema.Types.ObjectId, ref: 'Feedback'}]
});

// User.path('roles').validate(validateRoles, 'Please select at least one role, mentor or learner.')

User.plugin(passportLocalMongoose, {
  usernameField: "email"
});

User.methods.is = function (role) {
  return this.roles.indexOf(role) != -1;
};

User.methods.similarityTo = function(that) {

  var me = this.analysis, them = that.analysis;
  if (!me || !them) return 0;

  function sim(prop) { return 1 - Math.abs(me[prop] - them[prop]); }
  var similarity = (sim('openness') + sim('neuroticism') + sim('agreeableness') +
    sim('extraversion') + sim('conscientiousness')) / 5;

  return similarity;
};

User.virtual('mentor').get(function() {
  return this.is('mentor');
});

User.virtual('learner').get(function() {
  return this.is('learner');
});

User.virtual('likesCount').get(function() {
  return this.likes ? this.likes.length : "Not found";
});

User.virtual('isEmailVerified').get(function(){
  return true;
  //return this.emailVerification && this.emailVerification.isVerified ? this.emailVerification.isVerified : false;
});

User.virtual('approved').get(function(){
  return this.isApproved;
});

User.virtual('getPublicData').get(function(){
  //return user profile public data fields

  var data = {
    '_id' : this._id,
    'email' : this.email,
    'learner' : this.learner,
    'mentor' : this.mentor,
    'fullName' : this.fullName,
    'avatar' : this.avatar,
    'aboutme' : this.aboutme,
    'subjects' : this.subjects,
    'fieldOfStudy' : this.fieldOfStudy,
    'skype' : this.skype,
    'facebook' : this.facebook,
    'calendly' : this.calendly,
    'linkedin' : this.linkedin,
    'analysis' : this.analysis,
    'comments' : this.comments,
    'secureNet' : this.secureNet
  }

  //convert analysis to percentage if there is any
  if (data.analysis)
    data.analysis = {openness : Math.floor(this.analysis.openness*100),
                     neuroticism : Math.floor(this.analysis.neuroticism*100),
                     agreeableness : Math.floor(this.analysis.agreeableness*100),
                     extraversion : Math.floor(this.analysis.extraversion*100),
                     conscientiousness : Math.floor(this.analysis.conscientiousness*100)};


  return data;
})

mongoose.model('User', User);

