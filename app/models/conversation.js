var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Message = new Schema({
  from: { type: Schema.Types.ObjectId, ref: 'User' },
  text: 'string',
  timestamp: 'date',
});

mongoose.model('Message', Message);

var Conversation = new Schema({
  participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  messages: [{type: Schema.Types.ObjectId, ref: 'Message'}]
});

mongoose.model('Conversation', Conversation);

