var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    auth = require('../helpers/auth'),
    crypto = require('crypto'),
    path = require('path'),
    async = require('async'),
    email = require('../helpers/email');

module.exports = function (app) {
  app.use('/', router);
};

// --- INDEX

router.get('/',function(req,res){
  res.sendFile(path.resolve('public/main.html'));
})

// ---- SIGNUP

function performSignup(req, res, next) {

  console.log(req.body);

  var password = req.body.password,
      roles = [];

  if (req.body.mentor) roles.push('mentor');
  if (req.body.learner) roles.push('learner');

  var newUser = new User({
    credit : 60*30, //free 30 min call credit for everyone who signs up
    email: req.body.email,
    fullName: req.body.fullName,
    avatar : '/img/profile_placeholder.png',
    roles: roles,
    isApproved : false,
    emailVerification : {
      isVerified : false
    }
  });

  User.register(newUser, password, function (err) {
    console.log(err);
    if (err) return res.status(500).send({errors : err.message});
    else next();
  });
}

router.post('/signup', auth.notLoggedIn, performSignup, passport.authenticate('local'), function (req, res) {

  User.populate(req.user,{path : "comments",
                          populate : {path : "from"}
                         },
          function(err,user){
            if (err) return res.status(500).send({errors : err.message});
            else if(!user.isApproved) return res.status(500).send({errors : 'User has to be approved by admins'});
            else{
              return res.status(200).send(user.getPublicData);
            }
          }); 
});

// ---- LOGIN


router.post('/login', auth.notLoggedIn,passport.authenticate('local'), function(req, res) {
  User.populate(req.user,{path : "comments",
                          populate : {path : "from"}
                         },
          function(err,user){
            if (err) return res.status(500).send({errors : err.message});
            else{
              return res.status(200).send(user.getPublicData);
            }
          }); 
});

router.get('/is_logged', auth.loggedIn, function (req, res) {
  return res.status(200).end();
});


// ---- LOGOUT
router.get('/logout', function(req, res) {
  req.logout();
  res.status(200).end();
});


// ---- PASSWORD RESET
router.get('/forgot', function(req,res){
    res.render('auth/forgot');
});

router.get('/forgot/:token/', function(req,res){

  if (req.params.token !== undefined || req.params.token !== null){
    User.findOne({$and : [{'passwordReset.token' : req.params.token},
                          {'passwordReset.expiration' : {$gt : Date.now()}}]}, 
                  function(err, user){
      if (err){
        return res.redirect('/#/forgot');
      } 
      else if (user) {
        return res.redirect('/#/forgot/password_reset?token=' + req.params.token);
      }
      else {
        return res.redirect('/#/forgot');
      }
    });

  }else{
    return res.redirect('/#/forgot');
  }

});

router.post('/forgot/:token/', function(req,res){
  if (req.params.token !== undefined || req.params.token !== null){
    User.findOne({$and : [{'passwordReset.token' : req.params.token},
                          {'passwordReset.expiration' : {$gt : Date.now()}}]
                        }, 
                  function(err, user){
      if (err) return res.status(500).send(err);
      else if (user) {
        //update user password
        if (req.body.newPassword === req.body.reNewPassword){
          user.setPassword(req.body.newPassword,function(){
            user.passwordReset.expiration = Date.now() - 1;
            user.passwordReset.token = null;
            user.save(function(err){
              if (err) next(err);
              else {
                return res.status(200).send({infos : "New password successfully set."});
              }
            });
          });
        }else{
          return res.redirect('/#/forgot/password_reset?token=' + req.params.token);
        }        
      }
      else {
        return res.status(401).send({errors : "Unauthorized"});
      }
    });
  }else{
    return res.status(500).send({errors : "bad request"});
  }
});
router.post('/forgot' , function(req,res,next){

  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
          // req.flash('error', 'No account with that email address exists.');
          return res.status(401).send({errors : "no account with that email address"});
        }

        user.passwordReset.token = token;
        user.passwordReset.expiration = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      console.log("sending email");
      var text = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/#/forgot/password_reset?token=' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n';

      email.send(user.email, "notification@collegeconnections.co.uk" , text , "Password Reset" 
        , function(err,info){
        if (err) {
          // req.flash('error', "Couldn't send the email. Try again.");
          // console.log(err);
           return res.redirect("/#/forgot");
        }
        else {
          // req.flash('info' , "Check your email");
          done(err);
        }
      });
    }
  ], function(err) {
    if (err) return next(err);

    // res.status(200).send('email sent');
    return res.redirect("/#/forgot/email_success");
  });

});


// ---- EMAIL VERIFICATION

router.get('/is_email_verified', auth.loggedIn, function (req, res) {

  User.findOne({_id : req.user._id},function(err,user){
    if (err) return res.status(500).end();
    else if (!user) return res.status(401).send({errors : "Unauthorized"});
    else return res.status(200).send({verified : user.isEmailVerified, email : user.email});
  });

});


router.post('/email_verification', auth.loggedIn, function (req, res) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ _id : req.user._id }, function(err, user) {
        if (!user) {
          // req.flash('error', 'No account with that email address exists.');
          return res.status(401).send({errors : "no account with that email address"});
        }

        if (req.body.email !== req.user.email) user.email = req.body.email;

        if(!user.emailVerification) user.emailVerification = {};

        user.emailVerification.token = token;
        user.emailVerification.expiration = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var text = 'You are receiving this because you need to verify the email for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/#/email_verification/verify?token=' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n';

      email.send(user.email, "notification@collegeconnections.co.uk" , text , "Email Verification" 
        , function(err,info){
        if (err) {
          // req.flash('error', "Couldn't send the email. Try again.");
          // console.log(err);
           return res.redirect("/#/email_verification");
        }
        else {
          // req.flash('info' , "Check your email");
          done(err);
        }
      });
    }
  ], function(err) {
    if (err) return next(err);

    return res.status(200).send({infos : "email sent"});
  });
 
});

router.get('/email_verification/:token', function(req,res){

  console.log(req.params.token);
   User.findOne({$and : [{'emailVerification.token' : req.params.token},
                          {'emailVerification.expiration' : {$gt : Date.now()}}]
                        }, 
                  function(err, user){

    if (err) {
      console.log(err);
      return res.status(500).send({errors : err.message});
    }
    else if (user) {
      user.emailVerification.expiration = Date.now() - 1;
      user.emailVerification.token = null;
      user.emailVerification.isVerified = true;
      user.save(function(err){
        if (err) return res.status(500).send({errors : err.message});
        else return res.status(200).send({infos : "Email address verified"});
      });
    }
    else {
      return res.status(401).send({errors : "Unauthorized"});
    }

  });
});


// --- NOTIFICATIONS

router.get('/notifications', auth.notificationLogin, function (req, res) {
 
    User.findOne({_id : req.user._id},function(err,user){
      if (err) next(err);
      else res.send(req.user._id);
    });

});





