var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Conversation = mongoose.model('Conversation'),
    Message = mongoose.model('Message'),
    User = mongoose.model('User'),
    async = require('async'),
    auth = require('../helpers/auth'),
    notificationHelper = require('../helpers/notification.js');

module.exports = function (app) {
  app.use('/messages', router);
};

// Routes

router.get('/', auth.loggedIn, function (req, res) {
  var currentUserId = req.user._id;
  Conversation.find({ participants: currentUserId })
              .populate({path : 'participants', 
                          populate : {path : 'from'}})
              .exec(function (err, convos) {
    if (err) return next(err);
    else {
      var convoViewModels = convos.map(function (c) {
        // TODO assumes only 2 participants in any convo
        var from = c.participants.find(function (p) { return p._id != String(currentUserId) });
        var timestamp = c.messages[0] ? c.messages[0].timestamp : null;
        return { id: c._id, from: from, timestamp: timestamp }
      });
      res.status(200).send({ convos: convoViewModels });
    }
  });
});

router.get('/request', auth.loggedIn, function (req, res) {
  
  var participants = [ req.query.targetId, req.user._id ];
  Conversation.findOne({ participants: participants }, function (err, convo) {
    if (err) return next(err);
    else if (convo) return res.status(200).send({convo : convo});
    else Conversation.create({ participants: participants }, function (err, createdConvo) {
      if (err) return console.log(err);
      else {
        User.find({$or : [{_id : participants[0]},
                          {_id : participants[1]}
                          ]},
                 function(err,users){
          if (err) return console.log(err);
          else {
            async.each(users,
              function(item,done){
                item.conversations.push(createdConvo._id);
                item.save(function(err){
                  if (err) return console.log(err);
                  else return done();
                })
              },
              
              function(err){
                if (err) return console.log(err);
                else res.status(200).send({convo : createdConvo});
              }
            );
          }
        })
      }
    });
  });
});

router.get('/notifications', auth.loggedIn, function (req, res) {

  User.findOne({_id : req.user._id},function(err,user){
    if (err) next(err);
    else res.send(req.user._id);
  })

})

router.get('/:convoId', auth.loggedIn, function (req, res) {

  var convoId = req.params.convoId;

  Conversation.findOne({ _id: convoId })
              .populate({ path: 'messages', 
                          populate: { path: 'from'}
                        })
              .exec(function (err, convo) {

    if (err) return next(err);
    else if (convo){
      res.status(200).send({convo : convo});
    }
    else next("Could not find conversation.");
  });
});

router.post('/:convoId/send', auth.loggedIn, function (req, res, next) {

  var convoId = req.params.convoId;
  var text = req.body.message;

  console.log(text);

  Conversation.findOne({ _id: convoId }, function (err, convo) {
    if (err) return next(err);
    else {
      Message.create({from: req.user._id, text: text, timestamp: new Date() }, function(err,message){
        if (err) return next(err);
        else {
          convo.messages.push(message);
          convo.save(function (err) {
            if (err) return next(err);
            else {
              var to = convo.participants.find(function (p) { return p != String(req.user._id) });
              Message.findOne(message)
                     .populate({path : 'from'})
                     .exec(function(err,message){
                if (err) next(err);
                else {
                  console.log("sending " + text + " to " + to + " from " + req.user._id);

                  // check if the socket is alive
                  // if its not alive then send an email as notification
                  // and add it to notification count

                  notificationHelper.getIO().sockets.in(to).emit('message', {message : message , id: convo._id});
                  
                  //store the data -> will be deleted on message seen
                  var count = notificationHelper.ioGetMessageCount(to,convo._id);
                  console.log("sent");
                  if (count)
                    count++;
                  else
                    count = 1;

                  notificationHelper.ioStoreMessageCount(to, convo._id, count);
                  return res.status(200).send(message);
                }
                
              });
              
            }
          });
        }
      })
      
    }
  });
});
