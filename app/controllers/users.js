var _ = require('lodash'),
    express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Feedback = mongoose.model('Feedback'),
    Comment = mongoose.model('Comment'),
    auth = require('../helpers/auth'),
    profileHelper = require('../helpers/profile'),
    async = require('async');


var feedbackQuestions = [{question : 'How easy was it to organize this mentoring session?'},
    {question : 'How punctual and prepared was this person for your mentoring session?'},
    {question : 'How clear was this person’s communication?'},
    {question : 'How was this person’s subject knowledge?'},
    {question : 'How well did you get on with this person?'}];


module.exports = function (app) {
  app.use('/users', router);
};

// Routes

function paginate(offset, limit, end) {
  function genUrl(o) { return ('/users/?offset=' + o + '&limit=' + limit); }
  var prev = offset - limit, next = offset + limit;
  return {
    prev: prev < 0 ? null : genUrl(prev),
    next: end ? null : genUrl(next)
  }
}

function addSimilarity(user, docs) {
  console.log("adding similarity");
  docs.forEach(function (doc) {
    doc.similarity = doc.similarityTo(user);
  });
}

router.post('/', auth.loggedIn, function (req, res, next) {


    //if role = mentor 
  //  -> matching learners with field of study
  //  -> matching mentors with subjects
  //if role = learner 
  //   -> matching mentors with subjects
  //   -> matching learners with field of study
  //if role = learner + mentor 
  //    -> matching mentors with subjects
  //    -> matching learners with field of study
  var filters;

  if (Object.keys(req.body).length > 0){
   filters = {
    view : parseInt(req.body.view),
    filterInterest : req.body.filterInterests,
    filterProfile : req.body.filterProfile
    };
  }else{
    filters = {
      view : 2,
      filterInterest : false,
      filterProfile : false,
    };
  }


  var offset = parseInt(req.query.offset) || 0, limit = parseInt(req.query.limit) || 20;

  console.log(req.body);


  var query = {$and : []};
  var filterQuery;
  var filterQueryOptions = [];
  var viewQuery;
  //view query
  switch(filters.view){
    case 0:
      viewQuery = [{roles : 'learner'}];
      filterQueryOptions = ['fieldOfStudy'];
      break;
    case 1:
      viewQuery = [{roles : 'mentor'}];
      filterQueryOptions = ['subjects'];
      break;
    case 2:
      viewQuery = [{$or : [{roles : 'mentor'}, {roles : 'learner'}]}];
      filterQueryOptions = ['subjects','fieldOfStudy'];
      break;
  }

  //filtering
  if (!filters.filterInterest && !filters.filterProfile){
    filterQuery = [{_id: { $ne: req.user._id }}, 
                   {likes: { $exists: true }}
                  ];
  }else{
    filterQuery = { $and : [
                {_id: { $ne: req.user._id }}, 
                {likes: { $exists: true }}
            ]};

    if (filters.filterProfile){
      filterQuery.$and = filterQuery.$and.concat(profileHelper.isCompleteQuery);

    }
    if (filters.filterInterest){
      var orQuery = {$or : []};
    
      if (req.user.mentor && req.user.learner){
        for (var i=0;i<req.user.fieldOfStudy.length;i++){
          orQuery.$or.push({'fieldOfStudy' : req.user.fieldOfStudy[i]});
          orQuery.$or.push({'subjects' : req.user.fieldOfStudy[i]});
          
        }
        for (var i=0;i<req.user.subjects.length;i++){
          orQuery.$or.push({'fieldOfStudy' : req.user.subjects[i]});
          orQuery.$or.push({'subjects' : req.user.subjects[i]});
        }

      }else if (req.user.learner){
        for (var i=0;i<req.user.fieldOfStudy.length;i++){
          orQuery.$or.push({'fieldOfStudy' : req.user.fieldOfStudy[i]});
          orQuery.$or.push({'subjects' : req.user.fieldOfStudy[i]});
        }

      }else{
        for (var i=0;i<req.user.subjects.length;i++){
          orQuery.$or.push({'fieldOfStudy' : req.user.subjects[i]});
          orQuery.$or.push({'subjects' : req.user.subjects[i]});
        }
      }

      if (orQuery.$or.length > 0)
        filterQuery.$and.push(orQuery);
    }
  }

  query.$and = viewQuery.concat(filterQuery);

  User.find(query, null, { skip: offset, limit: limit }, function (err, docs) {

    if (err) return next(err);

    // docs.forEach(function (doc) {
    //   doc.similarity = doc.similarityTo(req.user);
    // });


    // console.log(docs);

    var templateArgs = {
      users: docs,
      links: paginate(offset, limit, docs.length < limit)
    };

    res.status(200).send(templateArgs);
  });
});


router.get('/:id/view', auth.loggedIn, function (req, res, next) {
  var id = req.params.id;
  User.findOne({ _id: id })
      .populate({path : "feedbacks",
                 populate : {path : "from"}
               })
      .populate(
              {path : "comments",
                populate : {path : "from"}
              })
      .exec(function (err, user) {
    if (err) next(err);
    else {
      user.similarity = user.similarityTo(req.user);
      var data = user.getPublicData;
      data.similarity = user.similarity;
      res.status(200).send(data);
    }
  });
});

// router.get('/:id/feedback', auth.loggedIn, function (req, res, next) {
//   var id = req.params.id;
//   //first find user and populate feedback info
//   User.findOne({ _id: id })
//       .populate({
//                 path : 'feedbacks',
//                 populate: { path:  'from'}
//               })
//       .exec(function(err,user){
//         if (err) return next(err);
//         else {
//           //now find my feedback
//           Feedback.find({from : req.user.id , to : user.id})
//                   .populate('from')
//                   .exec(function(err,myFeedback){
//                       if (err) return next(err);
//                       else {
//                         var params = {profile : user, 
//                                       myFeedback: myFeedback || null ,
//                                       feedbackQuestions : feedbackQuestions};

//                         return res.render('user/feedback', params);
//                       }
//               });
//         }
//     });
// });

router.post('/:id/feedback', auth.loggedIn, function (req, res, next) {
  var id = req.params.id;

  User.findOne({ _id: id }, function (err, user) {
    if (err) next(err);
    else {

      var newFeedback = {
        to : user.id,
        from : req.user.id,
        timestamp : new Date(),
        questions : []
      }

      for (var i=0;i<req.body.question.length;i++){
        newFeedback.questions.push({answer : req.body.answer[i],
                                    question : req.body.question[i],
                                    rating : req.body.rating[i]});
      }

      Feedback.create(newFeedback,function(err,createdFeedback){
        user.feedbacks.push(createdFeedback);
        user.save(function(err){
          if (err) return next(err);
          else{
            return res.redirect('/users/' + id + '/feedback');
          }
        });
      });
    }
  });
});

router.post('/:id/comment', auth.loggedIn, function (req, res, next) {
  var id = req.params.id;
  User.findOne({ _id: id }, function (err, user) {
    if (err) next(err);
    else {
      var newComment = {
        text: req.body.text, from: req.user, timestamp: new Date()
      };
      Comment.create(newComment, function (err, comment) {
        if (err) next(err);
        else {
          user.comments.unshift(comment);
          user.save(function (err) {
            if (err) next(err);
            else return res.redirect('/users/' + id + '/view');
          });
        }
      });
    }
  });
});

router.get('/match', auth.loggedIn, function (req, res, next) {

  var query = { analysis: { $exists: true } , _id : {$ne : req.user._id}};

  if (req.user.roles.length > 0) {
    query.roles = req.user.mentor ? "learner" : "mentor";

    if (req.user.subjects.length > 0 || req.user.fieldOfStudy.length > 0){
      query.$or = [];
      if (query.roles === "learner"){
        for (var i=0;i<req.user.subjects.length;i++)
          query.$or.push({'fieldOfStudy' : req.user.subjects[i]});

      }else{
        for (var i=0;i<req.user.fieldOfStudy.length;i++)
          query.$or.push({'subjects' : req.user.fieldOfStudy[i]});
      }
    }
  }

  console.log(query);

  User.find(query, function (err, docs) {

    console.log(docs);

    if (err) return next(err);
    if (docs.length < 1) return res.status(500).send("Not enough users to match with!");

    addSimilarity(req.user, docs);
    var matched = _.first(_.sortBy(docs, function (u) { return u.similarity; }));
    res.status(200).send({id : matched._id});
  })
})
