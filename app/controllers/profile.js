var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    auth = require('../helpers/auth'),
    web = require('../helpers/web'),
    async = require('async'),
    payos = require('../helpers/securenet').getPayOS();

module.exports = function (app) {
  app.use('/profile', router);
};

function analysisToPercentage(analysis){

  if (analysis)
    return {openness : Math.floor(analysis.openness*100),
          neuroticism : Math.floor(analysis.neuroticism*100),
          agreeableness : Math.floor(analysis.agreeableness*100),
          extraversion : Math.floor(analysis.extraversion*100),
          conscientiousness : Math.floor(analysis.conscientiousness*100)};
  else return null;
}


// Routes

router.get('/', auth.loggedIn, function (req, res) {

  User.populate(req.user,{path : "comments",
                          populate : {path : "from"}
                         },
          function(err,user){
            if (err) return next(err);
            else{
              return res.send(user.getPublicData);
            }
          }); 
 });

router.get('/billing',auth.loggedIn,function(req,res,next){
  
  if (req.user.secureNet){
    console.log("checking for billing information");
    payos.getCustomer(parseInt(req.user.secureNet.customerId),function(err,info){
      console.log(info);
      if (!info.success) return res.status(500).send({errors : info.message});
      else if (info.customerId === 'ERROR') {
        //customer deleted from worldpay side

        User.update({_id : req.user._id},{secureNet : null},function(err){
          if (err) return res.status(500).send({errors : err.message});
          else return res.status(404).end();
        });

      }
      else return res.status(200).send(info);
    });
  }else{
    return res.status(404).end();
  }
});

router.get('/payment',auth.loggedIn,function(req,res,next){
 

    //get all the payment methods using async and send it all when done
    // async.
    var paymentMethods = [];
    console.log(req.user.secureNet.paymentMethodIds);

    User.findOne({_id : req.user._id},function(err,user){

      if(err) return res.status(500).send({errors : err.message});
      else if(!user) return res.status(401).send({errors : "Unauthorised"});
      else {
          console.log(user.secureNet);
         if (user.secureNet && user.secureNet.customerId){
           async.each(user.secureNet.paymentMethodIds,
            function(item,done){
              console.log(item);
 
              var data = {customerId : user.secureNet.customerId,
                                          paymentMethodId : item};
              payos.getCustomerPaymentMethod(data,
              function(err,info){
                if (!info.success) done();
                else {
                  paymentMethods.push(info.vaultPaymentMethod);
                  return done();
                }
              });
          },
          function(err){
            if (err) return res.status(500).send({errors : err.message});
            else return res.status(200).send(paymentMethods);
          });

        }else return res.status(404).end();

      }
    });
});


router.post('/payment',auth.loggedIn,function(req,res,next){

  //payment method creation

  if (req.user.secureNet){
    //create the payment method
    req.body.customerId = req.user.secureNet.customerId;

    payos.createCustomerPaymentMethod(req.body,function(err,info){
      if(!info.success) return res.status(500).send({errors : info.message});
      else {

        User.findOne({_id : req.user._id},function(err,user){
          if (err) return res.status(500).send({errors : err.message});
          else if (!user) return res.status(404).send({errors : "user not found"});
          else {
            if (!user.secureNet.paymentMethodIds) user.secureNet.paymentMethodIds = [];

            
            user.secureNet.paymentMethodIds.push(info.vaultPaymentMethod.paymentId)
            user.save(function(err){

              if (err) return res.status(500).send({errors : err.message});
              else return res.status(200).send({payment : info , infos : "Payment method created"});
            })
            
          }
        })
        
      }
    });
  }else{
    //create new user
    return res.status(404).send({errors : "user account doesn't exist"});

  }

});

router.get('/payment/:id/delete',auth.loggedIn,function(req,res,next){

  if (req.user.secureNet && req.user.secureNet.customerId){

    payos.deleteCustomerPaymentMethod({paymentMethodId : req.params.id,
                                       customerId : req.user.secureNet.customerId},

                              function(err,info){
      if(!info.success) return res.status(500).send({errors : info.message});
      else {
        return res.status(200).send({infos : "Payment method deleted"});
      }
    });
    
  }else{
   
    return res.status(404).send({errors : "user account doesn't exist"});

  }

});


router.post('/edit', auth.loggedIn, function (req, res, next) {

  var roles = [];
  if (req.body.mentor) roles.push('mentor');
  if (req.body.learner) roles.push('learner');

  User.findOne({ _id: req.user._id }, function (err,user) {
    if (err) return res.status(500).send({errors : err.message});
    else if(!user) return res.status(401).send("Unauthorised");
    else {
      user.fullName = req.body.fullName;
      user.email = req.body.email;
      user.roles = roles;
      user.skype = req.body.skype;
      user.calendly = req.body.calendly;
      user.aboutme = req.body.aboutme;
      user.subjects = req.body.subjects || [];
      user.fieldOfStudy = req.body.fieldOfStudy || [];

      user.save(function(err){
        if (err) return res.status(500).send({errors : err.message});
        else res.status(200).send(user.getPublicData);
      });

    }
      
  });

});

// router.get('/edit/password', auth.loggedIn, function (req, res) {
//   res.render('profile/password' , {profile : req.user});
// });


router.post('/edit/password',auth.loggedIn,function(req,res,next){
  // generate the authenticate method and pass the req/res
  passport.authenticate('local', function(err, user, info) {
    if (err) return res.status(500).send({errors : err.message}); 
    else if(!user) return res.status(401).send({errors : "Unauthorized"});
    else {
       user.setPassword(req.body.newPassword,function(){
                user.save(function(err){
                  if (err) return res.status(500).send({errors : err.message});
                  else return res.status(200).send({infos : "Password successfully changed"});
                });
              });
    }

  })(req, res, next);

});

router.post('/edit/billing',auth.loggedIn,function(req,res,next){
  console.log("editing billing");
  if (req.user.secureNet && req.user.secureNet.customerId){
    //update the billing 
    console.log("updating billing");
    payos.updateCustomer(req.body,function(err,info){
      if(!info.success) return res.status(500).send({errors : info.message});
      else return res.status(200).send({infos : "Billing updated"});
    });
  }else{
    //create new user
    console.log("creating new billing");

    payos.createCustomer(req.body, function(err,info){
      if(!info.success) return res.status(500).send({errors : info.message});
      else {
        console.log(info);
        User.update({_id : req.user._id},{secureNet  : {customerId : info.customerId}},function(err,user){
          if(err) return res.status(500).send({errors : err.message});
          else if(!user) return res.status(401).send({errors : 'Unauthorized'});
          else res.status(200).send({infos : "Billing created"});
        });
      }
    });

  }

});

router.post('/edit/payment/:id',auth.loggedIn,function(req,res,next){

  if (req.user.secureNet && req.user.secureNet.customerId){
    //update the payment method
    req.body.customerId = req.user.secureNet.customerId;
    req.body.paymentMethodId = req.params.id;
    payos.updateCustomerPaymentMethod(req.body,function(err,info){
      if(!info.success) return res.status(500).send({errors : info.message});
      else {
        return res.status(200).send({payment : info.vaultPaymentMethod , infos : "Payment method updated"});
      }
    });
  }else{
    //create new user
    return res.status(404).send({errors : "user account doesn't exist"});

  }

});
      

