var express = require('express'),
    router = express.Router(),
    auth = require('../helpers/auth'),
    personality = require('../helpers/personality');

module.exports = function (app) {
  app.use('/personality', router);
};

function pluckTraitValue(predictions, trait) {
  return predictions.find(function (p) {
    return p.trait === trait;
  }).value;
}

// Routes

router.get('/', auth.facebookConnected, function (req, res) {
  personality.auth(function (err, authToken) {
    if (err) res.status(403).send("Personality API auth error");
    else personality.analysis(authToken, req.user, function (err, results) {
      if (err) {
        if (err.status === 204) {
          res.redirect('/#/profile');
        } else {
          res.status(500).send(err);
        }
      } else {

        var predictions = results.predictions;
        var analysis = {
          openness: pluckTraitValue(predictions, 'BIG5_Openness'),
          neuroticism: pluckTraitValue(predictions, 'BIG5_Neuroticism'),
          agreeableness: pluckTraitValue(predictions, 'BIG5_Agreeableness'),
          extraversion: pluckTraitValue(predictions, 'BIG5_Extraversion'),
          conscientiousness: pluckTraitValue(predictions, 'BIG5_Conscientiousness')
        };

        req.user.analysis = analysis;
        req.user.save(function (err) {
          if (err) res.status(500).send(err);
          else res.redirect('/#/profile');
        });
      }
    });
  });
});
