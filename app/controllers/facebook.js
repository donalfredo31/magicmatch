var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    auth = require('../helpers/auth'),
    fbHelper = require('../helpers/facebook');

module.exports = function (app) {
  app.use('/facebook', router);
};

// Routes

router.get('/', passport.authenticate('facebook', { scope: ['email', 'user_likes']}));

router.get('/callback', passport.authenticate('facebook', {
  successRedirect: '/#/profile', failureRedirect: '/#/login'
}));

router.get('/disconnect', auth.facebookConnected, function (req, res, next) {
  fbHelper.disconnect(req.user, function (err) {
    if (err) return next(err);
    else res.redirect('/#/profile');
  });
});
