var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    async = require('async'),
    auth = require('../helpers/auth')
    payos = require('../helpers/securenet').getPayOS();


var products = [
		{
		 	name : 'Pay-as-you-go Individual',
		 	id : 1,
		 	type : 'payasyougo',
		 	details : 'Pay-as-you-go deal with £65 per hour when bought individually',
		 	price : 65,
		 	currency : 'gbp',
		 	expiration : null,
		 	credit : 60*60 //1 hour
		},
		{
		 	name : 'Pay-as-you-go Block',
		 	id : 2,
		 	type : 'payasyougo',
		 	details : 'Pay-as-you-go deal with £45 per hour when bought in a block of 10 hours',
		 	price : 45*10,
		 	currency : 'gbp',
		 	expiration : null,
		 	credit : 10*60*60 //10 hours
		},
		{
			name : 'Structured Package',
			id : 3,
		 	type : 'installment',
		 	details : 'Fully structured long-term service. £2000 paid in three instalments over 12 months.',
		 	price : 2000,
		 	repeat : 3,
		 	currency : 'gbp',
		 	expiration : 12,
		 	credit : 60*60 //1 hour
		}
	];

module.exports = function (app) {
  app.use('/services', router);
};

// Routes

router.get('/', function (req, res) {

	return res.status(200).send(products);
  
});

router.get('/:id', auth.loggedIn, function (req, res) {

	var product = products.find(function(item){return item.id == req.params.id});
	console.log(product);

	if (product)
		return res.status(200).send(product);
  else 
  	return res.status(500).send({errors : 'product not found'});

});

router.post('/:id/purchase', auth.loggedIn, function (req, res) {

	//find the product
	var product = products.find(function(item){return item.id == req.params.id});

	if (product){
		var data = {};


		//find the user first then charge
		User.findOne({_id : req.user._id},function(err,user){

			if (err) return res.status(500).send({errors : err.message});
			else if(!user) return res.status(401).send({errors : "unauthorised"});
			else {
				switch (product.type){
					case 'payasyougo':

						//var paymentToken = user.secureNet.paymentMethods.find(function(item){return item.paymentId == req.body.paymentId}).token;

						data.amount = product.price;
						data.paymentVaultToken = {
							paymentMethodId : req.body.paymentId,
							customerId : user.secureNet.customerId
						}
						
						payos.charge(data,function(err,info){
							console.log(err);
							console.log(info);
							if(info.success){
								user.credit = user.credit + product.credit;
								user.save(function(err){
									if (err) return res.status(500).send({errors : err.message});
									else return res.status(200).send({infos : "successfully charged"});
								})
							}else{
								console.log(info);
								return res.status(500).send({errors : info.message});
							}
						})
						break;

					case 'installment':
						var date = new Date();

						data.customerId = user.secureNet.customerId;
						data.plan = {
							cycleType: 'monthly',
							frequency: 4,
							numberOfPayments: 3,
							dayOfTheMonth: 1,
    						dayOfTheWeek: 1,
							installmentAmount: product.price,
							startDate: date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear(),
							endDate: date.getMonth() + "/" + date.getDate() + "/" + (date.getFullYear()+1),
							maxRetries: 4,
							primaryPaymentMethodId: req.body.paymentId
						};

						console.log(data);
						payos.createInstallmentPlan(data,function(err,info){
							console.log(err);
							console.log(info);
							if(info.success){
								user.credit = user.credit + product.credit;
								user.save(function(err){
									if (err) return res.status(500).send({errors : err.message});
									else return res.status(200).send({infos : "successfully charged"});
								})
							}else{
								console.log(info);
								return res.status(500).send({errors : info.message});
							}
						})
						break;

				}
			}

		})



		

	}else{
		return res.status(404).send({errors : "product not found"});
	}


	
 
});


