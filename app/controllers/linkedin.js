var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    auth = require('../helpers/auth'),
    linkedin = require('../helpers/linkedin');

module.exports = function (app) {
  app.use('/linkedin', router);
};

// Routes

router.get('/', passport.authenticate('linkedin', { scope: ['r_basicprofile', 'r_emailaddress']}));

router.get('/callback', passport.authenticate('linkedin', {
  successRedirect: '/#/profile/edit', failureRedirect: '/#/profile' }));

router.get('/disconnect', auth.linkedinConnected, function (req, res, next) {
  linkedin.disconnect(req.user, function (err) {
    if (err) return next(err);
    else res.redirect('/#/profile');
  });
});
