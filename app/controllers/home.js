var express = require('express'),
    router = express.Router();

module.exports = function (app) {
  app.use('/', router);
};

// Routes

// router.get('/', function (req, res) {
//   res.render('index');
// });

// router.get('/privacy', function (req, res) {
//   res.render('privacy');
// });
