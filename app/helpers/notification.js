var uuid = require('node-uuid'),
		async = require('async');

var CALL_MAX_PEERS = 2;

var ioData = {};
var io;

var stream_rooms = {};

module.exports = {

	init : function(http){

		console.log("initializing socket io");
		io = require( "socket.io" )(http);

		io.on('connection',function(socket){
			console.log('a user connected');
			var currentCall = null;

			socket.on('subscribe', function(id){
				console.log("user " + id + "subscribed to notification channel");
				socket.join(id);

				//stored notifications
				if (ioData[id]){
					console.log(ioData[id]);
					Object.keys(ioData[id]).forEach(function(k){
							io.sockets.in(id).emit(k,ioData[id][k]);
    			});
				}else{
					ioData[id] = {};
					
				}
			});

			socket.on('disconnect', function(){
	    	console.log('user disconnected');

	    	if (currentCall != null && stream_rooms[currentCall]){
	    		console.log("deleting socket from " + currentCall);

	    		var itemIndex = stream_rooms[currentCall].findIndex(function(item) { return item.socket == socket });
	    		
	    		socket.leave(currentCall);
	    		io.sockets.in(currentCall).emit('peer_disconnected', { user: stream_rooms[currentCall][itemIndex].user});

	    		stream_rooms[currentCall].splice(itemIndex,1);
	    		if (stream_rooms[currentCall].length <= 0) delete stream_rooms[currentCall];

	    		currentCall = null;

		    }else{
		    	return;
		    }

	  	});

			socket.on('unsubscribe', function(id) {  
	      console.log("user " + id + "unsubscribed from notification channel");
	      socket.leave(id); 
	    });

	    socket.on('call_end',function(data,fn){

	    	if (currentCall != null && stream_rooms[currentCall]){
	    		console.log("deleting socket from " + currentCall);

	    		var itemIndex = stream_rooms[currentCall].findIndex(function(item) { return item.socket == socket });
	    		
	    		socket.leave(currentCall);
	    		io.sockets.in(currentCall).emit('peer_disconnected', { user: stream_rooms[currentCall][itemIndex].user});

	    		stream_rooms[currentCall].splice(itemIndex,1);
	    		currentCall = null;

		    }else{
		    	return;
		    }
	    });

	    socket.on('call_request',function(data,fn){
	    	console.log(io.sockets.adapter.rooms[data.to._id]);
	    	// //find if user is online
	    	if(io.sockets.adapter.rooms[data.to._id] && io.sockets.adapter.rooms[data.to._id] !== 'undefined'){
	    		//check if user is busy
	    		io.sockets.in(data.to._id).emit('call_request',{from : data.from});
	    	}else{
	    		console.log("unavailable");
	    		io.sockets.in(data.from._id).emit('call_unavailable',{});
	    	}

	    	

	    });

	    socket.on('call_answer',function(data,fn){
	    	switch (data.answer){
	    		case "accept":
	    			//create the room and send the id 
	    			var roomId = uuid.v4();

	    			io.sockets.in(data.to._id).emit('call_accepted',{room : roomId});
	    			io.sockets.in(data.from._id).emit('call_accepted',{room : roomId});
	    			break;

	    		case "reject":
	    			io.sockets.in(data.to._id).emit('call_request_rejected',{});
	    			io.sockets.in(data.from._id).emit('call_rejected',{});
	    			break;
	    	}
	    });

	    socket.on('call_cancel',function(data,fn){
	    	
    			io.sockets.in(data.to._id).emit('call_request_cancelled',{});
    			io.sockets.in(data.from._id).emit('call_cancelled',{});
	    		
	    });

	    socket.on('call_init', function (data, fn) {

	    	console.log(stream_rooms);
	      var roomId = (data || {}).room || uuid.v4();
	      var user = data.from;

	      //check if the room exists or being created
	      if (!stream_rooms[roomId]) {

	        stream_rooms[roomId] = [{socket : socket , user : user}];
	        currentCall = roomId;
	        fn(roomId);
	        console.log('Call created, with #', roomId);

	        //join the created call
	        currentCall = roomId;
	        socket.join(currentCall);

	      } else {
	        if (!stream_rooms[roomId] || stream_rooms[roomId].length >= CALL_MAX_PEERS ) {
	          return;
	        }
	       	
	       	if (currentCall == roomId){
	       		//already in the room
	       		return;	
	       	}

	       	//join the call
	       	currentCall = roomId;
	       	

	        fn(roomId);
	        stream_rooms[roomId].push({socket : socket , user : user}); // add the user id to the room
	        console.log('User ' + user.fullName + ' connected to call ', roomId);
	        io.sockets.in(currentCall).emit('peer_connected', { room: roomId });
	        socket.join(currentCall);
	      }
	    });

	    socket.on('call',function(args){
	    	//relays data between streams -> constraint to 2 people
	    	console.log("relaying call data");
	    	console.log(args);
	    	socket.broadcast.to(args.room).emit('call',args);
		    // io.sockets.to(args.to).broadcast('call', args);

	    });

			socket.on('data_seen' , function(args){

				
				var clearMessageCount = function(socketID,convoID){
					if (ioData[id]['message_count'])
						if (ioData[id]['message_count'][convoID])
	    				ioData[id]['message_count'][convoID] = {};
				}

				var dataType = args.dataType,
	    			id = args.id,
	    			data = args.data;

	    	if (dataType === 'message_count'){
	    		console.log("CLEARING message_count for socket " + args.id + " and for convo " + args.data.id);
	    		clearMessageCount(args.id,args.data.id);
	    	}

			});

	    socket.on('data_success' , function(args){
	    	// switch(args.type){
	    	// 	case "call_request":

	    	// 		break;
	    	// }
	    	console.log("socket io data received by client");
	    });

		});
	},

	getIO : function(){
		return io;
	},

	ioStoreMessageCount : function(socketID,convoID,count){


		if (!ioData[socketID]['message_count']){
			ioData[socketID]['message_count'] = {};
		}

		ioData[socketID]['message_count'][convoID] = count;



	},

	ioGetMessageCount : function(socketID,convoID){

		if (ioData[socketID]['message_count'])
			if (ioData[socketID]['message_count'][convoID])
				return ioData[socketID]['message_count'][convoID];
			else
				return null;
		else 
			return null;
	},

	ioStoreData : function(socketID,dataType,data){

		console.log("storing socket io data")

		ioData[socketID][dataType] = data;
		

		
	},

	ioGetData : function(socketID,dataType){
		if (ioData[socketID]){
			return ioData[socketID][dataType];
		}else{
			return null;
		}
	},

	ioClearData : function(socketID){
		if (ioData[socketID])
			ioData[socketID] = {};
	}
}