module.exports = {

  notLoggedIn: function (req, res, next) {
    if (req.user) res.redirect('/');
    else next();
  },

  loggedIn: function (req, res, next) {
    if (req.user && req.user.isApproved) next();
    else res.status(401).send({errors : "Wrong credentials. Please try again."});
  },

  emailVerified : function(req,res,next){
    if (req.user && req.user.emailVerification && req.user.emailVerification.isVerified) next();
    else res.status(401).send({errors : "Please verify your email"});
  },

  notificationLogin : function(req, res, next){
    if (req.user) next();
    else res.status(403).end();
  },

  role: function (role) {
    return function (req, res, next) {
      if (req.user && req.user.is(role)) next();
      else res.status(403).end();
    }
  },

  facebookConnected: function (req, res, next) {
    if (req.user) {
      if (!req.user.facebook) res.status(403).send("Please connect your Facebook account");
      else next();
    } else res.redirect('/login');
  },

  linkedinConnected: function (req, res, next) {
    if (req.user) {
      if (!req.user.linkedin) res.status(403).send("Please connect your LinkedIn account");
      else next();
    } else res.redirect('/login');
  }
}
