var nodeMailer = require('nodemailer');
var emailTransporter = require('../../config/email')(nodeMailer);

var send = function(to,from,text,subject,callback){

	var mailOptions = {
		to : to,
		from : from,
		text : text,
		subject : subject
	};

	emailTransporter.sendMail(mailOptions, function(error, info){
    if(error){
        return callback(error,null);
    }
    return callback(null,info);
	});
}

module.exports = {
	send : send
}