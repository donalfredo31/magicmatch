var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    async = require('async');



var tests = {
	'avatarCheck' : function(){ //insert the ones that don't have avatars
				User.update({$or : [{avatar: { $exists: false }} , {avatar : ""}]},
										{avatar : "/img/profile_placeholder.png"},
										{multi:true}, 
										function(err){
											return;
										});
								},
	// 'commentCheck' : function(){
	// 	Comment.update({$or : [{avatar: { $exists: false }} , {avatar : ""}]},
	// 									{avatar : "/img/profile_placeholder.png"},
	// 									{multi:true}, 
	// 									function(err){
	// 										return;
	// 									});
	// 							},
	// },
	'creditCheck' : function(){
			User.update({credit: { $exists: false }},
										{credit : 30*60},
										{multi:true}, 
										function(err){
											return;
										});
	},
	'testUserCreation' : function(){ // create users for test

		var CONFIG = {
			userNum : 100
		}

	}


}

var runTests = function(testNames) {

	console.log("running tests");

	if (testNames && testNames.length > 0){
		for (var i=0;i<testNames.length;i++){
			tests[testNames[i]]();
		}
	}else{
		Object.keys(tests).forEach(function(k){
    	tests[k]();
  	});
	}

}

module.exports = {
	runTests : runTests
};


