var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.model('User');


var searchFilters = {
    view : 2,
    filterInterests : null,
    filterProfile : null
}

module.exports = {

	isCompleteQuery : [{roles : {$exists : true , $ne : []}},
                 {fullName : {$exists : true , $ne : ""}},
                 {aboutme : {$exists : true , $ne : ""}},
                 {avatar : {$exists : true , $ne : ""}},
                 {analysis : {$exists : true}},
                 {$or : [{fieldOfStudy : {$exists : true , $ne : []}},
                 					{subjects : {$exists : true , $ne : []}}]
                 }],
	isComplete : function(user){
		

	},

	hasField : function(user,fieldName,fieldType){

	},

    setSearchFilters : function(newFilters) {
        searchFilters = newFilters;
        console.log(searchFilters);
    },

    getSearchFilters : function(){
        return searchFilters;
    }

}