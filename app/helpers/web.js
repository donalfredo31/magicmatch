function flashErrorsAndRedirect(path) {
  return function (err, req, res, next) {
    if (err.errors) {
      var fields = Object.keys(err.errors);
      fields.forEach(function (k) { req.flash('error', err.errors[k].message); });
      res.redirect(path);
    } else next(err);
  }
}

module.exports = {
  flashErrorsAndRedirect: flashErrorsAndRedirect
}
