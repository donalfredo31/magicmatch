var request = require('superagent');

var baseUrl = 'http://api-v2.applymagicsauce.com',
    customerId = '2299',
    apiKey = '46gbdm9kqn37770hiftgbd1tuk';

function auth(cb) {
  request.post(baseUrl + '/auth')
         .accept('application/json')
         .send({ customer_id: customerId, api_key: apiKey })
         .end(function (err, res) {
           if (err || !res.ok) {
             console.log("Auth error: " + JSON.stringify(err));
             cb(err);
           } else {
             cb(null, res.body.token);
           }
         });
}

// TODO: wrap calls in this
function withAuth(next) {

  return function () {

    var handleAuthError = function (err, result) {
      if (err && (err.status === 401 || err.status === 403)) {
        auth(function (err) {
          if (err) console.log("Cannot complete request due to auth error.");
          else {
            var originalArgs = argsWithoutCb.concat([originalCb]);
            withAuth(next).apply(null, originalArgs);
          }
        });
      } else {
        originalCb(err, result);
      }
    };

    var argsWithoutCb = Array.prototype.slice.call(arguments);
    var originalCb = argsWithoutCb.pop;
    var argsWithAuthCallback = argsWithoutCb.concat([handleAuthError]);

    next.apply(null, argsWithAuthCallback);
  }
}

function analysis(authToken, user, cb) {
  request.post(baseUrl + '/like_ids')
         .accept('application/json')
         .set({ 'X-Auth-Token': authToken })
         .query({ traits: 'BIG5', uid: user.facebook })
         .send(user.likes)
         .end(function (err, res) {
           if (err || !res.ok) {
             cb(err);
           } else if (res.status === 204) {
             cb({ status: 204, message: "Insufficient likes to provide a personality analysis" });
           } else {
             cb(null, res.body);
           }
         });
}

module.exports = {
  auth: auth,
  analysis: analysis
}
