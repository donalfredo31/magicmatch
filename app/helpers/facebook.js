var mongoose = require('mongoose'),
    fbagent = require('fbagent'),
    async = require('async');

var User = mongoose.model('User');

function extractLikeIds(likesData) {
  return likesData.map(function (like) { return like.id; });
}

function getAllLikeIds(accessToken, profile, cb) {

  console.log(profile._json.likes || profile._json);
  var likes = profile._json.likes;
  if (!likes) return cb(null, []);

  var likeIds = extractLikeIds(likes.data);

  async.whilst(
    function () { return likes.paging && likes.paging.next; },
    function (cb) {
      var nextUrl = likes.paging.next.substr(26); // remove abs url
      fbagent.token(accessToken)
             .get(nextUrl)
             .end(function (err, payload) {
               if (err) return cb(err);
               else {
                 likes = payload;
                 likeIds = likeIds.concat(extractLikeIds(payload.data))
                 cb(null);
               }
             });
    },
    function (err) {
      if (err) return cb(err);
      else cb(null, likeIds);
    }
  );
}

function updateLikes(user, fbData, cb) {
  getAllLikeIds(fbData.accessToken, fbData.profile, function (err, likeIds) {
    user.likes = likeIds;
    user.save(cb);
  });
}

function linkAccount(user, fbData, cb) {
  user.facebook = fbData.profile.id;
  user.avatar = fbData.profile.photos ? fbData.profile.photos[0].value : '/img/profile_placeholder.png'
  updateLikes(user, fbData, cb);
}

function registerWithFacebook(fbData, cb) {

  var profile = fbData.profile;
  User.collection.insert({
    facebook: profile.id,
    fullName: profile.displayName,
    email: profile.emails[0].value,
    isApproved : false,
    emailVerification :{
      isVerified : true
    },
    avatar : profile.photos ? profile.photos[0].value : '/img/profile_placeholder.png',
    roles: []
  }, function (err, result) {
    if (err) cb(err);
    else {
      var user = User.hydrate(result.ops[0]);
      updateLikes(user, fbData, cb);
    }
  });
}

function disconnect(user, cb) {
  user.update({ $unset: { facebook: "", likes: "", analysis: "" }}, cb);
}

module.exports = {
  linkAccount: linkAccount,
  registerWithFacebook: registerWithFacebook,
  updateLikes: updateLikes,
  disconnect: disconnect
}
