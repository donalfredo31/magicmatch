var mongoose = require('mongoose'),
    User = mongoose.model('User');

function extractDetails(profile) {
  console.log(profile);
  return { id: profile.id, url: profile._json.siteStandardProfileRequest.url };
}

function linkAccount(user, profile, cb) {
  user.linkedin = extractDetails(profile);
  user.save(cb);
}

function register(profile, cb) {
  User.collection.insert({
    linkedin: extractDetails(profile),
    fullName: profile.displayName,
    avatar : '/img/profile_placeholder.png',
    isApproved : false,
    email: profile.emails[0].value,
     emailVerification :{
      isVerified : true
    },
    roles: []
  }, function (err, result) {
    if (err) return cb(err);
    else {
      var user = User.hydrate(result.ops[0]);
      return cb(null, user);
    }
  });
}

function disconnect(user, cb) {
  user.update({ $unset: { linkedin: "" }}, cb);
}

module.exports = {
  linkAccount: linkAccount,
  register: register,
  disconnect: disconnect
}
