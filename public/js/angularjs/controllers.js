MentorMatch.controller('CallCtrl',[
    '$sce',
    'VideoStream',
    '$stateParams',
    '$scope',
    'Call',
    '$state',
    function ($sce, VideoStream, $stateParams, $scope, Room,$state) {

    if (!window.RTCPeerConnection || !navigator.getUserMedia) {
      $scope.error = 'WebRTC is not supported by your browser. You can try the app with Chrome and Firefox.';
      return;
    }

    $scope.peer = null;

    var stream;

    VideoStream.get()
    .then(function (s) {
      stream = s;
      Room.init(stream);
      stream = URL.createObjectURL(stream);
      if (!$stateParams.id) {
        Room.createRoom()
        .then(function (roomId) {
          $state.go('call', {id : roomId});
        });
      } else {
        Room.joinRoom($stateParams.id);
      }
    }, function () {
      $scope.error = 'No audio/video permissions. Please refresh your browser and allow the audio/video capturing.';
    });
    $scope.peer;
    $scope.$on('peer_stream', function (event,peer) {
      console.log('Client connected, adding new stream');
      console.log(peer.stream);
      $scope.peer = {
        id: peer.id,
        stream: URL.createObjectURL(peer.stream)
      };

    });
    $scope.$on('peer_disconnected', function (event,peer) {
      console.log('Client disconnected, removing stream');
      $scope.peer = null;
    });

    $scope.getLocalVideo = function () {
      return $sce.trustAsResourceUrl(stream);
    };

     $scope.getRemoteVideo = function () {
      return $sce.trustAsResourceUrl($scope.peer.stream);
    };

}]);


MentorMatch.controller('ServiceController',[
    '$scope',
    '$state',
    'API',
    '$stateParams',
    function($scope,$state,API,$stateParams){

        $scope.errors;
        $scope.infos;

        switch($state.current.name){
            case 'services':
                $scope.services = [];
                //get the products 
                API.getServices(function(response){
                    $scope.services = response.data;
                },function(response){
                    $scope.errors = response.data.errors;
                    console.log(response);
                });

                break;

            case 'service':
                $scope.service = {};

                if(!$stateParams.id)
                    $state.go('services');
                else{
                    API.getService($stateParams.id,function(response){
                        $scope.service = response.data;
                    },function(response){
                        $scope.errors = response.data.errors;
                        console.log(response);
                    });
                }
                break;

            case 'service_purchase':
                $scope.paymentMethods = [];
                $scope.hasAccount = false;
                $scope.loading = true;
                $scope.billing = {};
                $scope.payment = {};

                if(!$stateParams.id)
                    $state.go('services');
                else{
                    API.getBillingDetails(function(response){
                        $scope.payment.billing = response.data.vaultCustomer;
                        $scope.hasAccount = true;

                        API.getPaymentMethods(function(response){
                            $scope.hasPaymentMethod = true;
                            $scope.loading = false;
                            $scope.paymentMethods = response.data;
                            
                        },function(response){
                            $scope.loading = false;
                            $scope.hasPaymentMethod = false;
                        });

                    },function(response){
                        //user has to be created first
                        $scope.hasAccount = false;
                        $scope.hasPaymentMethod = false;
                        $scope.loading = false;
                    });
                }


                $scope.buy = function(){

                    if ($scope.payment.method){
                        API.buyService($stateParams.id,$scope.payment.method,function(response){
                            $scope.infos = response.data.infos;
                        },function(response){
                            $scope.errors = response.data.errors;
                            console.log(response);
                        });
                    }
                    
                }
                break;
        }
    }
]);


MentorMatch.controller('PaymentEditController',[
    '$scope',
    '$state',
    'API',
    'UserService',
    'payOS',
    function($scope,$state,API,UserService,payOS){

        $scope.errors = {};
        $scope.infos = {};

        $scope.form = {}

        $scope.loading = true;

        $scope.hasAccount = false;
        $scope.hasPaymentMethod = false;
        $scope.isEditingPayment = false;
        $scope.isAddingPayment = false;

        $scope.expirationMonth;
        $scope.expirationYear;

        $scope.paymentMethods = [];

        //get the user payment details - billing and method
       

        API.getBillingDetails(function(response){
            $scope.form.billing = response.data.vaultCustomer;
            $scope.hasAccount = true;

            API.getPaymentMethods(function(response){
                $scope.hasPaymentMethod = true;
                $scope.loading = false;
                $scope.paymentMethods = response.data;
                
            },function(response){
                $scope.loading = false;
                $scope.hasPaymentMethod = false;
            });

        },function(response){
            //user has to be created first
            $scope.hasAccount = false;
            $scope.hasPaymentMethod = false;
            $scope.loading = false;
        });

        $scope.initPayment = function(){
             $scope.form.paymentMethod = {

                primary : true,
                phone : "",
                card : {
                    email : "",
                    number : "",
                    expirationDate : "",
                    cvv : "",
                    address : {
                        line1 : "",
                        company : "",
                        city : "",
                        state : "",
                        country : "",
                        zip : ""
                    },
                    firstName : "",
                    lastName : ""

                }
            };
        }

        $scope.initBilling = function(){
            $scope.form.billing =  {
                firstName : "",
                lastName : "",
                emailAddress : "",
                sendEmailReceipts : false,
                phoneNumber : "",
                address : {
                            line1 : "",
                            company : "",
                            city : "",
                            state : "",
                            country : "",
                            zip : ""
                            }
            };
        }

        $scope.paymentContactCheckbox = function(isChecked){

            if (isChecked){
                $scope.form.paymentMethod.phone = $scope.form.billing.phoneNumber;
                $scope.form.paymentMethod.card.email = $scope.form.billing.emailAddress;
                $scope.form.paymentMethod.card.firstName = $scope.form.billing.firstName;
                $scope.form.paymentMethod.card.lastName = $scope.form.billing.lastName;
                $scope.form.paymentMethod.card.address = $scope.form.billing.address;
            }

        }

        $scope.saveBilling = function(){

            $scope.errors = {};
            $scope.infos = {};

            API.setBillingDetails($scope.form.billing,function(response){
                $scope.infos.billing = response.data.infos;
                $scope.hasAccount = true;

            },function(response){
                $scope.errors.billing = response.data.errors;
                $scope.hasAccount = false;
            });
        } 

        $scope.editPaymentMethod = function(index){

            if(index >= 0){
                $scope.isEditingPayment = true;
                $scope.form.paymentMethod = $scope.paymentMethods[index];
            }else{
                $scope.isAddingPayment = true;
                $scope.initPayment();
            }

        }


        $scope.deletePaymentMethod = function(index){

            $scope.errors = {};
            $scope.infos = {};

            API.deletePaymentMethod($scope.paymentMethods[index].paymentId,function(response){
                $scope.paymentMethods.splice(index,1);
                $scope.infos.payment = response.data.infos;
            },function(response){
                $scope.errors.payment = response.data.errors;
            });
        }

        $scope.savePaymentMethod = function(){
           
            $scope.errors = {};
            $scope.infos = {};

            if($scope.isEditingPayment){

                API.setPaymentMethod($scope.form.paymentMethod,$scope.form.paymentMethod.paymentId,function(response){

                    $scope.paymentMethods.splice($scope.paymentMethods.indexOf($scope.form.paymentMethod),1);
                    $scope.paymentMethods.push(response.data.payment);

                    $scope.infos.payment = response.data.infos;
                    $scope.isEditingPayment=false;

                },function(response){
                    $scope.errors.payment = response.data.errors;
                });

            }else if ($scope.isAddingPayment){
                API.createPaymentMethod($scope.form.paymentMethod,function(response){
                    console.log(response.data);
                    $scope.infos.payment = response.data.infos;
                    $scope.paymentMethods.push(response.data.payment.vaultPaymentMethod);
                    $scope.isAddingPayment = false;
                    $scope.hasPaymentMethod = true;

                },function(response){
                    $scope.errors.payment = response.data.errors;
                });
            }

        } 
    
        $scope.goBack = function(){
            $scope.isEditingPayment=false;
            $scope.isAddingPayment=false;
        }

        $scope.initBilling();
        $scope.initPayment();

    }
]);




MentorMatch.controller('ForgotController',[
    '$scope',
    '$state',
    function($scope,$state){

        $scope.errors = $state.params.errors;
        $scope.infos = $state.params.infos;
    }
]);

MentorMatch.controller('EmailVerificationController',[
    '$scope',
    '$state',
    'UserService',
    'API',
    function($scope,$state,UserService,API){
       

       $scope.errors;
       $scope.infos;

       console.log($state.params);

       if ($state.params.token){
        console.log($state.params.token);
        API.verifyEmail($state.params.token,function(response){
            $scope.infos = response.data.infos;
            $state.go('index');
        },function(response){
            $scope.errors = response.data.errors;
        });
       }else{
            $scope.email = UserService.getUser().email;
       }

       $scope.sendEmailVerification = function(){
        console.log($scope.email);
            API.sendEmailVerification({email : $scope.email},function(response){
                $scope.infos = response.data.infos;
            },function(response){
                $scope.errors = response.data.errors;
            });
       };
    }
]);



MentorMatch.controller('AuthController',[
    '$scope',
    'API',
    '$state',
    'UserService',
    '$stateParams',
    function($scope,API,$state,UserService,$stateParams){

        console.log($stateParams);
        console.log($state.params);

        $scope.errors;
        $scope.infos;

        $scope.login = function(data){

            API.login(data,function(response){
                $state.go('profile');
            },function(response){
                $scope.errors = response.data;
            });
        }

        $scope.signup = function(data){

            API.signup(data,function(response){

                //go to profile
                $state.go('profile');

            },function(response){
                console.log(response.data.errors);
                $scope.errors = response.data.errors;
            });
            
        }

        $scope.facebookLogin = function(data){

            API.facebookLogin(data,function(response){
                //go to profile
                $state.go('profile');

            },function(response){
                console.log(response);
            });
        }

        $scope.linkedinLogin = function(data){

            API.linkedinLogin(data,function(response){
                //go to profile
                $state.go('profile');
            },function(response){
                console.log(response);
            });
  
        }

        $scope.resetPassword = function(data){

            API.resetPassword($state.params.token,data.newPassword,data.reNewPassword,function(response){
                $scope.infos = response.data.infos;
            },function(response){
                $scope.errors = response.data.errors;
            });
        }

    }
]);

MentorMatch.controller('IndexController',[
    '$scope',
    'API',
    'UserService',
    '$state',
    function($scope,API,UserService,$state){

        $scope.user = null;

        if (UserService.isUser()){
            $scope.user = UserService.getUser();
        }else{
            API.getProfile(function(response){
                $scope.user = response.data;
            },function(response){

            })
        }

        $scope.$on('login',function(event,args){
            $scope.user = UserService.getUser();
        });

        $scope.$on('logout',function(event,args){
            $scope.user = null;
        });

    }
]);

MentorMatch.controller('ProfileController',[
    '$scope',
    'API',
    'UserService',
    '$state',
    function($scope,API,UserService,$state){

        $scope.user = null;

        if (UserService.isUser()){
            $scope.user = UserService.getUser();
        }

        $scope.$on('login',function(event,args){
            $scope.user = args.user;
        });

    }
]);

MentorMatch.controller('PasswordEditController',[
    '$scope',
    'API',
    '$state',
    '$stateParams',
    function($scope,API,$state,$stateParams){

        $scope.errors;
        $scope.infos;

        $scope.resetPassword = function(data){
            API.resetPassword($stateParams.token,data.newPassword,data.reNewPassword,function(response){
                $state.go('login');
            },function(response){
                $scope.errors = response.data.errors;
            });
        }

        $scope.forgotPassword = function(email){

            API.forgotPassword(email,function(response){
                alert(response.data);
            },function(response){
                alert(response.data);
            });
        }

        
    }
]);

MentorMatch.controller('ProfileEditController',[
    '$scope',
    'API',
    'UserService',
    '$state',
    'halos',
    function($scope,API,UserService,$state,halos){

        $scope.user;
        $scope.educationData = halos.haloDatabase;

        $scope.errors;
        $scope.infos;

        if (UserService.isUser()){
            $scope.user = UserService.getUser();

        }

        $scope.$on('login',function(event,args){
            $scope.user = args.user;
        });

        //edit stuff

        $scope.addFieldOfStudy = function(field){
            console.log($scope.formData);
            if ($scope.user.fieldOfStudy.indexOf(field) < 0)
                $scope.user.fieldOfStudy.push(field);
        }

        $scope.removeFieldOfStudy = function(index){
            $scope.user.fieldOfStudy.splice(index,1);
        }

         $scope.addSubject = function(subject){
            if ($scope.user.subjects.indexOf(subject) < 0)
                $scope.user.subjects.push(subject);
        }

        $scope.removeSubject = function(index){
            $scope.user.subjects.splice(index,1);
        }

        $scope.saveUser = function(){

            //send new user to server
            API.editProfile($scope.user,function(response){
                //redirect to profile

                $state.go('profile');
            },function(response){
                console.log(response);
                $scope.errors = response.data.errors;
            });
        }

        $scope.changePassword = function(data){
            console.log(data);
            API.setPassword($scope.user.email,data.oldPassword,data.newPassword,function(response){
                $state.go('profile_edit');
            },function(response){
                console.log(response.data);
                $scope.errors = response.data.errors;
            });
        }
    }
]);

MentorMatch.controller('UserController',[
    '$scope',
    '$state',
    '$stateParams',
    'API',
    'socket',
    'UserService',
    'ModalService',
    function($scope,$state,$stateParams,API,socket,UserService,ModalService){

        $scope.user = null;

        if (!$stateParams.id){
            //show list
            $state.go('user_list');
        }else{
            //get user details
            API.getUser($stateParams.id,function(response){
                $scope.user = response.data;
            },function(response){
                $state.go('login');
            });
        }

        $scope.sendMessage = function(id){

            API.createConversation(id,function(response){
                console.log(response);
                $state.go('conversation',{id : response.data.convo._id});
            },function(response){

            });  
        }

        $scope.call = function(){
           socket.emit('call_request',{from : UserService.getUser(),to : $scope.user});
           //create the call modal
           ModalService.open({
                animation: true,
                component: 'callRequestModal',
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                backdrop: false,
                size : 'lg',
                resolve: {
                  user: function () {
                    return $scope.user;
                  },
                  title : function(){
                    return "Requesting video call";
                  },
                  text : function(){
                    return "is being called";
                  }
                }
            },'call_request',function (result) {


            }, function () {


            
            });
           
        }
    }
]);

MentorMatch.controller('UserListController',[
    '$scope',
    '$state',
    'API',
    function($scope,$state,API){

        $scope.users = null;
        $scope.links;
        $scope.filters = {
            view : 2,
            filterInterest : false,
            filterProfile : false
        };

        //get user details
        API.getUserList("/users",$scope.filters,function(response){
            $scope.links = response.data.links;
            $scope.users = response.data.users;

        },function(response){
            $state.go('login');
        });
        

        $scope.changePage = function(link){
            API.getUserList(link,$scope.filters,function(response){
                $scope.links = response.data.links;
                $scope.users = response.data.users;

            },function(response){

            });
        }

        $scope.filter = function(){
            API.getUserList("/users",$scope.filters,function(response){
                $scope.links = response.data.links;
                $scope.users = response.data.users;
                console.log($scope.users);

            },function(response){

            });
        }

    }
]);

MentorMatch.controller('InboxController',[
    '$rootScope',
    '$scope',
    'NotificationService',
    'API',
    'UserService',
    '$state',
    function($rootScope,$scope,NotificationService,API,UserService,$state){

        $scope.user;
        $scope.conversations = [];
        $scope.unseenMessageCount = {};

        $scope.getUnseenCount = function(id){
            return $scope.unseenMessageCount[id];
        }

        $scope.getInbox = function(){
            API.getInbox(function(response){
                $scope.conversations = response.data.convos;
                $scope.unseenMessageCount = NotificationService.getMessageCount();
                console.log($scope.unseenMessageCount);
            },function(reponse){

            });
        }

        if (UserService.isUser()){
            $scope.user = UserService.getUser();
            $scope.getInbox();
        }

        $scope.$on('login',function(event,args){
            $scope.user = args.user;
            $scope.getInbox();
        });

        $scope.$on('message_count' , function(event,args){
            $scope.unseenMessageCount[args.id] = args.count;
        });

        $scope.$on('message' , function(event,args){
            $scope.unseenMessageCount[args.id] = NotificationService.getTotalMessageCount(args.id);
        });

        $scope.$on('message_seen' , function(event,args){
            $scope.unseenMessageCount[args.id] = NotificationService.getTotalMessageCount(args.id);
        });
    }
]);

MentorMatch.controller('HeaderController',[
    '$rootScope',
    '$scope',
    'NotificationService',
    'API',
    'UserService',
    '$state',
    function($rootScope,$scope,NotificationService,API,UserService,$state){

        $scope.headerMessageCount = 0;
        $scope.user = null;
        

        $scope.$on('message_count' , function(event,args){
            $scope.headerMessageCount = NotificationService.getTotalMessageCount();
        });

        $scope.$on('message' , function(event,args){
            $scope.headerMessageCount = NotificationService.getTotalMessageCount();
            console.log($scope.headerMessageCount);
        });

        $scope.$on('message_seen' , function(event,args){
            $scope.headerMessageCount = NotificationService.getTotalMessageCount();
        });

        $scope.$on('login' , function(event,args){
            $scope.user = args.user;
            console.log($scope.user);
            $scope.headerMessageCount = NotificationService.getTotalMessageCount();

        });

        $scope.matchMe = function(){
            API.match(function(response){
                $state.go('user',{id : response.data.id})
            },function(response){
                alert(response.data);
            })
        }

        $scope.logout = function(){

            API.logout(function(response){
                $scope.user = null;
                $state.go('index');
            },function(response){
                console.log(response);
            });

        }
    }
]);

MentorMatch.controller('ConversationController',[
    '$rootScope',
    '$scope',
    '$stateParams',
    '$state',
    'NotificationService',
    'UserService',
    'API',
    function($rootScope,$scope,$stateParams,$state,NotificationService,UserService,API){

        NotificationService.clearMessages($stateParams.id);
        $scope.conversation = {};
        $scope.user;

        $scope.getConversation = function(convoID){
            API.getConversation(convoID,function(response){
                $scope.conversation = response.data.convo;
                console.log($scope.conversation);
            },function(response){

            });
        }

        if (UserService.isUser()){
            $scope.user = UserService.getUser();
            $scope.getConversation($stateParams.id);
        }

        $scope.sendMessage = function(message){
            $scope.text = "";
            API.sendMessage(message,$stateParams.id,function(response){
                console.log(response);
                $scope.conversation.messages.push(response.data);
            },function(response){   
                console.log(response);
            });
        }


        $scope.$on('login',function(event,args){
            $scope.user = args.user;
            $scope.getConversation($stateParams.id);
        });


        $scope.$on('message' , function(event,args){
            if (args.id === $stateParams.id){
                if (Array.isArray(args.message)){
                    for (var i=0;i<args.message.length;i++)
                        $scope.conversation.messages.push(args.message[i]);
                }
                else
                    $scope.conversation.messages.push(args.message);

                NotificationService.clearMessages($stateParams.id);
            }

        });
    }
]);

