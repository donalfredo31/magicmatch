ConnectionModule.constant('halos',{
    haloDatabase : [

    {name : 'Academic',
     subjects : ['Accounting','Ancient History','Anthropology','Arabic','Archaeology','Architecture','Art','Bengali','Biology','Business','Studies','Chemistry','Chinese','Classical Civilisation','Classical Greek','Classics','Computing','Critical Thinking','Design','Divinity','Drama And Theatre Studies','Economics','Engineering','English Language','English Literature','French Literature','Geography','Geology','German Literature','Government And Politics','History','History Of Art','Italian Literature','Japanese Literature','Latin  Law','Mathematics','Medicine','Music Theory','Philosophy','Physics','Physiology','Portuguese Literature','Psychology','Russian Literature','Social Anthropology','Sociology','Spanish Literature','Statistics','Turkish Literature','Zoology']},
    {name : 'Business Skills',
     subjects : ['Accounting','Actuary','Charitable','E-commerce','Finance','Law','Liquidation','Marketing','Start-up Advice']},
    {name : 'Language Cafe',
     subjects : ['Arabic','Cantonese Chinese','Dutch','English','French','German','Hebrew','Hindi','Italian','Japanese','Korean','Malay','Mandarin Chinese','Portuguese','Russian','Spanish','Thai','Vietnamese'  ]},
    {name : 'Pursue Your Passion',
     subjects : [ 'Baseball','Basketball','Cello','Cooking','Cricket','Drawing','Driving','Drums','Flute','Football','Guitar','Horn','Interview Skills','Lacrosse','Piano','Programming','Rugby','Saxophone','Singing','Skateboarding','Skiing','Snowboarding','Surfing','Swimming','Trumpet','Viola','Violin','Wakeboarding']},

    ]
});

ConnectionModule.factory('VideoStream', function ($q) {
    var stream;
    return {
      get: function () {
        if (stream) {
          return $q.when(stream);
        } else {
          var d = $q.defer();
          navigator.getUserMedia({
            video: true,
            audio: true
          }, function (s) {
            stream = s;
            d.resolve(stream);
          }, function (e) {
            d.reject(e);
          });
          return d.promise;
        }
      }
    };
});

ConnectionModule.factory('Call', function ($rootScope, $q, socket,UserService) {

    var iceConfig = { 'iceServers': [{ 'url': 'stun:stun.l.google.com:19302' },
                                      {
                                        'url': 'turn:numb.viagenie.ca',
                                        'credential': 'longl1veA$AP',
                                        'username': 'alicansa@gmail.com'
                                      }
                            ]},
        peerConnections = {},
        stream;

    function getPeerConnection(room) {
      if (peerConnections[room]) {
        return peerConnections[room];
      }
      var pc = new RTCPeerConnection(iceConfig);
      peerConnections[room] = pc;
      pc.addStream(stream);
      pc.onicecandidate = function (evnt) {
          socket.emit('call', {room: room, ice: evnt.candidate, type: 'ice' });
      };
      pc.onaddstream = function (evnt) {
        console.log('Received new call');
        console.log(evnt);
        $rootScope.$broadcast('peer_stream', {
          room: room,
          stream: evnt.stream
        });
        if (!$rootScope.$$digest) {
          $rootScope.$apply();
        }
      };
      return pc;
    }

    function makeOffer(room) {
      var pc = getPeerConnection(room);
      console.log(pc);
      pc.createOffer(function (sdp) {
        pc.setLocalDescription(sdp);
        console.log('Creating an offer for', room);
        socket.emit('call', {room: room, sdp: sdp, type: 'sdp-offer' });
      }, function (e) {
        console.log(e);
      },
      { mandatory: { OfferToReceiveVideo: true, OfferToReceiveAudio: true }});
    }

    function handleMessage(data) {
      var pc = getPeerConnection(data.room);
      switch (data.type) {
        case 'sdp-offer':
          pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
            console.log('Setting remote description by offer');
            console.log(pc);
            pc.createAnswer(function (sdp) {
              pc.setLocalDescription(sdp);
              console.log("sending answer to " + data.room);
              socket.emit('call', {room: data.room, sdp: sdp, type: 'sdp-answer' });
            },function(e){
              console.log(e);
            });
          });
          break;
        case 'sdp-answer':
          pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
            console.log('Setting remote description by answer');
          }, function (e) {
            console.error(e);
          });
          break;
        case 'ice':
          if (data.ice) {
            console.log('Adding ice candidates');
            pc.addIceCandidate(new RTCIceCandidate(data.ice));
          }
          break;
      }
    }

    function addHandlers(socket) {
      socket.on('peer_connected', function (params) {
        console.log("peer connecting now making routing offers");
        makeOffer(params.room);
      });
      socket.on('peer_disconnected', function (data) {
        $rootScope.$broadcast('peer_disconnected', [data]);
      });
      socket.on('call', function (data) {
        console.log(data);
        handleMessage(data);
      });
    }

    var api = {
      joinRoom: function (r) {
        socket.emit('call_init', { room: r , from : UserService.getUser()}, function (roomid) {
          roomId = roomid;
        });
      },
      createRoom: function () {
        var d = $q.defer();
        socket.emit('call_init', {room : null , from : UserService.getUser()}, function (roomid) {
          d.resolve(roomid);
          roomId = roomid;
        });
        return d.promise;
      },
      init: function (s) {
        stream = s;
      }
    };
    // EventEmitter.call(api);
    // Object.setPrototypeOf(api, EventEmitter.prototype);

    addHandlers(socket);
    return api;
});


ConnectionModule.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

ConnectionModule.factory('payOS', function ($rootScope) {
    var addDetails = function(data){
        data.developerApplication =  {
            developerId: 12345678, // provided by SecureNet after certification 
            version: '1.2'
        }
        data.addToVault = true;
        data.publicKey = "e66d5d72-0651-4f73-b291-43890373500c";
    };


  return {
    tokenize: function (data,successCallback,errorCallback) {
        //use the the payOS.js to tokenize the card
        //once tokenized the success callback should fetch 
        // the new user payment details from the server
        addDetails(data);
        console.log(data);
        tokenizeCard(data).done(successCallback).fail(errorCallback);
    }
  };
});



ConnectionModule.service('UserService',[
    '$rootScope',
    function($rootScope){

        var user = null;

        this.isUser = function(){
            return user != null;
        }

        this.setUser = function(newUser){
            user = newUser;
            $rootScope.$broadcast('login',{user : user});
        }

        this.getUser = function(){
            return user;
        }

        this.clearUser = function(){
            user = null;
            $rootScope.$broadcast('logout');
        }

    }
]);

ConnectionModule.service('ModalService', [
    '$uibModal',
    function($uibModal){
      var modalInstances = [];

      this.open = function(options,modalName,promiseCallback,dismissCallback){

        modalInstances[modalName] = $uibModal.open(options);
        modalInstances[modalName].result.then(promiseCallback,dismissCallback);

      }

      this.close = function(modalName){

        //delete the instance
        modalInstances[modalName].close();
        delete modalInstances[modalName];

      }

      this.delete = function(modalName){

        //delete the instance
        delete modalInstances[modalName];

      }



    }
  ]);


ConnectionModule.service('NotificationService', [
    '$rootScope',
    'socket',
    'UserService',
    '$state',
    'ModalService',
    function($rootScope,socket,UserService,$state,ModalService){

        var messageCount = {};
        var channelID;


        this.subscribe = function(id){
            socket.emit('subscribe', id);
            this.setChannelID(id);
        }

        this.unsubscribe = function(id){
            socket.emit('unsubscribe',channelID);
            this.setChannelID(null);
        }

        this.setChannelID = function(id){
            channelID = id;
        }

        this.getChannelID = function(){
            return channelID;
        }

        this.clearMessages = function(convoID){

            if (channelID){
                socket.emit('data_seen',{id : channelID , 
                                         dataType : 'message_count',
                                         data : {id : convoID}});
            }else{
                
            }
           
            messageCount[convoID] = 0;

            $rootScope.$broadcast('message_seen', {id : convoID});
        }

        this.getMessageCount = function(){
            return messageCount;
        }

        this.getTotalMessageCount = function(id){
            if (id)
                return messageCount[id];
            else{
                var count=0;
                console.log(messageCount);
                Object.keys(messageCount).forEach(function(k){

                    count = count + messageCount[k];
                });
                return count;
            }  
        }

         // socket built in events
        socket.on('connect' , function(){
            console.log("socket connected");
        });

        socket.on('connecting' , function(){
            console.log("socket connecting");
        });

        socket.on('disconnect' , function(){
            console.log("socket disconnected");
        });

        socket.on('connect_failed' , function(){
            console.log("socket connect failed");
        });

        socket.on('error' , function(){
            console.log("socket error");
        });

        socket.on('reconnect' , function(){
            console.log("socket reconnected");
        });

        socket.on('reconnecting' , function(){
            console.log("socket reconnecting");
        });

        socket.on('reconnect_failed' , function(){
            console.log("socket reconnect failed");
        });

        //custom socket.io events

        socket.on('call_request',function(args){

          //create the call modal
          ModalService.open(
            {
              animation: true,
              component: 'callAnswerModal',
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              backdrop: false,
              size : 'lg',
              resolve: {
                user: function () {
                  return args.from;
                },
                title : function(){
                  return "Receiving video call";
                },
                text : function(){
                  return "is calling you";
                }
              }
            },'call_request',function (result) {

              socket.emit('call_answer',{
                answer : result,
                to : args.from,
                from : UserService.getUser()
              });

            }, function () {

           
            });

        });
        
        socket.on('call_rejected',function(args){
          //close the modal
          ModalService.close('call_request');

        });

        socket.on('call_unavailable',function(args){
          console.log("unavailable");
          //close the modal
          ModalService.close('call_request');
          alert("user unavailable");
        });


        socket.on('call_cancelled',function(args){
          //close the modal
          ModalService.close('call_request');

        });

         socket.on('call_request_cancelled',function(args){
          //close the modal
          ModalService.close('call_request');
          alert("call cancelled");
        });

         socket.on('call_busy',function(args){
          //close the modal
          ModalService.close('call_request');
          alert("call busy");
        });


        socket.on('call_request_rejected',function(args){
          //close the modal
          ModalService.close('call_request');
          alert("call rejected");
        });

        socket.on('call_accepted',function(args){
          //close the modal
          ModalService.close('call_request');

          // go to call page
          $state.go('call',{id : args.room});

        });

        socket.on('message' , function(args){

            if (!messageCount[args.id])
                messageCount[args.id] = 0;


            messageCount[args.id]++;

            $rootScope.$broadcast('message',{id : args.id , message : args.message});

            socket.emit('data_success',{id : channelID , 
                                        dataType : 'message',
                                        data : args.message});
        });

        socket.on('message_count' , function(args){
            console.log("message_count");
            console.log(args);

            Object.keys(args).forEach(function(k){
                messageCount[k] = args[k];
            });

            $rootScope.$broadcast('message_count',{id : args.id,
                                                   count : args.count});

            socket.emit('data_success',{id : channelID , 
                                        dataType : 'message_count',
                                        data : args.message});

        });
    }
]);

ConnectionModule.service('API',[
    '$rootScope',
    'ConnectionService',
    'NotificationService',
    'UserService',
    'socket',
    function($rootScope,ConnectionService,NotificationService,UserService){

        this.isLogged = function(success,failure){
            var requestProperties = {
                  url: "/is_logged",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.isEmailVerified = function(success,failure){
          var requestProperties = {
                  url: "/is_email_verified",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.sendEmailVerification = function(data,success,failure){
          var requestProperties = {
                  url: "/email_verification",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data

            };
            ConnectionService.postRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.verifyEmail = function(token,success,failure){
          var requestProperties = {
                  url: "/email_verification/" + token,
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });


        }

        this.login = function(data,success,failure){

            var requestProperties = {
                  url: "/login",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){
                NotificationService.subscribe(response.data._id);
                // UserService.setUser(response.data);
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.signup = function(data,success,failure){

            var requestProperties = {
                  url: "/signup",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){
                NotificationService.subscribe(response.data._id);
                // UserService.setUser(response.data);
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.facebookLogin = function(data,success,failure){

            var requestProperties = {
                  url: "/facebook",
                  dataType: 'jsonp',
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                NotificationService.subscribe(response.data._id);
                // UserService.setUser(response.data);
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.linkedinLogin = function(data,success,failure){

            var requestProperties = {
                  url: "/linkedin",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.getProfile = function(success,failure){
            var requestProperties = {
                  url: "/profile",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                NotificationService.subscribe(response.data._id);
                UserService.setUser(response.data);
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.editProfile = function(newUser,success,failure){

            var requestProperties = {
                  url: "/profile/edit",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : newUser
            };
            ConnectionService.postRequest(requestProperties,function(response){
                console.log(response.data);
                UserService.setUser(response.data);
                success(response);
            },function(response){
                failure(response);
            });


        }

        this.getUser = function(userID,success,failure){
            var requestProperties = {
                  url: "/users/" + userID + '/view',
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.createConversation = function(userID,success,failure){
            var requestProperties = {
                  url: "/messages/request?targetId=" + userID,
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.getConversation= function(convoID,success,failure){
            var requestProperties = {
                  url: "/messages/" + convoID,
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.getInbox = function(success,failure){
            var requestProperties = {
                  url: "/messages",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.getUserList = function(link,filters,success,failure){


            console.log(filters);

            var requestProperties = {
                  url: link,
                  headers: {
                      'Content-Type': 'application/json'
                  }, 
                  data : filters
            };
            ConnectionService.postRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.match = function(success,failure){
            var requestProperties = {
                  url: "/users/match",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.sendMessage = function(message,convoID,success,failure){
            var requestProperties = {
                  url: "/messages/" + convoID + "/send",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : {
                    message : message
                  }
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                console.log(response);
            });
        }

        this.resetPassword = function(token,newPassword,reNewPassword,success,failure){
            var requestProperties = {
                  url: "/forgot/" + token,
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : {
                    newPassword : newPassword,
                    reNewPassword : reNewPassword
                  }
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                console.log(response);
            });

        }

        this.forgotPassword = function(email,success,failure){
            var requestProperties = {
                  url: "/forgot",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : {
                    email : email
                  }
            };
            ConnectionService.postRequest(requestProperties,function(response){
                success(response);
            },function(response){
                failure(response);
            });
        }

        this.setPassword = function(email,oldPassword,newPassword,success,failure){
            var requestProperties = {
                  url: "/profile/edit/password",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : {
                    password : oldPassword,
                    email : email,
                    newPassword : newPassword
                  }
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.setBillingDetails = function(data,success,failure){
            var requestProperties = {
                  url: "/profile/edit/billing",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });

        }

        this.getBillingDetails = function(success,failure){
              var requestProperties = {
                  url: "/profile/billing",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.getPaymentMethods = function(success,failure){
             var requestProperties = {
                  url: "/profile/payment",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.createPaymentMethod = function(data,success,failure){

            var requestProperties = {
                  url: "/profile/payment/",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.setPaymentMethod = function(data,id,success,failure){

            var requestProperties = {
                  url: "/profile/edit/payment/" + id,
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }


        this.getPaymentMethod = function(id,success,failure){
            var requestProperties = {
                  url: "/profile/payment/" + id,
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.deletePaymentMethod = function(id,success,failure){

            var requestProperties = {
                  url: "/profile/payment/" + id + "/delete",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.pay = function(productId,data,success,failure){
            var requestProperties = {
                  url: "/purchase/" + productId,
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : data
            };
            ConnectionService.postRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.getServices = function(success,failure){
          var requestProperties = {
                  url: "/services",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){

                success(response);
                    
            },function(response){
                failure(response);
            });
        }

        this.getService = function(id,success,failure){
          var requestProperties = {
                  url: "/services/" + id,
                  headers: {
                      'Content-Type': 'application/json'
                  }
          };
          ConnectionService.getRequest(requestProperties,function(response){

              success(response);
                  
          },function(response){
              failure(response);
          });
        }

        this.buyService = function(id,card,success,failure){
          var requestProperties = {
                  url: "/services/" + id + "/purchase",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  data : card
          };
          ConnectionService.postRequest(requestProperties,function(response){

              success(response);
                  
          },function(response){
              failure(response);
          });
        }

        this.logout = function(success,failure){

            var requestProperties = {
                  url: "/logout",
                  headers: {
                      'Content-Type': 'application/json'
                  }
            };
            ConnectionService.getRequest(requestProperties,function(response){
                NotificationService.unsubscribe();
                UserService.clearUser();
                success(response);
            },function(response){
                failure(response);
            });

        }

    }
]);

ConnectionModule.service('ConnectionService', [
    '$http',
    function($http){

        //GET, POST, PATCH, DELETE REQUESTS

        this.request = function(requestProperties,sCallback,eCallback){
           $http(requestProperties).then(function successCallback(response) {
                sCallback(response);
            }, function errorCallback(response) {
                eCallback(response);
            });
        };

        this.getRequest = function(requestProperties,sCallback,eCallback){
            requestProperties.method = 'GET';
            this.request(requestProperties,sCallback,eCallback);
        };

        this.postRequest = function(requestProperties,sCallback,eCallback){

            requestProperties.method = 'POST';

            this.request(requestProperties,sCallback,eCallback);
        };

        this.patchRequest = function(requestProperties,sCallback,eCallback){

            requestProperties.method = 'PATCH';
            this.request(requestProperties,sCallback,eCallback);
        };

        this.deleteRequest = function(requestProperties,sCallback,eCallback){

            requestProperties.method = 'DELETE';
            this.request(requestProperties,sCallback,eCallback);
        };
    }
]);
