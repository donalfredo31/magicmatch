MentorMatch.directive("flash", [
    function () {
        return {
            restrict: 'E',
            scope : {
            	errors : "=",
                infos : "="
            },
            link: function(scope, element, attrs) {


            },
            templateUrl: 'templates/partials/flash.html'
        };

    }
]);

MentorMatch.directive('videoPlayer', function ($sce) {
    return {
      template: '<div><video ng-src="" autoplay></video></div>',
      restrict: 'E',
      replace: true,
      scope: {
        vidSrc: '@'
      },
      link: function (scope) {
        console.log('Initializing video-player');
        scope.trustSrc = function () {
          if (!scope.vidSrc) {
            return undefined;
          }
          return $sce.trustAsResourceUrl(scope.vidSrc);
        };
      }
    };
});

MentorMatch.component('callRequestModal', {
  templateUrl: 'templates/partials/call_modal_request.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function (socket,UserService) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
      $ctrl.user = $ctrl.resolve.user;
      $ctrl.text = $ctrl.resolve.text;
      $ctrl.title = $ctrl.resolve.title;

    };

    $ctrl.cancel = function () {
      socket.emit('call_cancel',
                  {from : UserService.getUser(),
                    to : $ctrl.user});
    };
  }
});


MentorMatch.component('callAnswerModal', {
  templateUrl: 'templates/partials/call_modal_answer.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function (socket,UserService) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
      $ctrl.user = $ctrl.resolve.user;
      $ctrl.text = $ctrl.resolve.text;
      $ctrl.title = $ctrl.resolve.title;

    };

    $ctrl.ok = function () {
       socket.emit('call_answer',{
                answer : 'accept',
                to : $ctrl.user,
                from : UserService.getUser()
              });
    };

    $ctrl.cancel = function () {
       socket.emit('call_answer',{
                answer : 'reject',
                to : $ctrl.user,
                from : UserService.getUser()
              });
    };
  }
});