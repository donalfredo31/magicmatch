
var MentorMatch = angular.module('MentorMatch', [

    // Third party module
    'ui.router',
    'ui.bootstrap',

    // Main modules
    'ConnectionModule'

    // External injections

]);

var ConnectionModule = angular.module('ConnectionModule', []);

MentorMatch.config(function($stateProvider,$httpProvider,$urlRouterProvider) {

  $urlRouterProvider.otherwise('/index');

  $stateProvider
    //---- INDEX ----

    .state('index',{
      url: '/index',
      templateUrl: 'templates/index.html',
      access : 'public'
    })

    //---- AUTH -----

    .state('login',{
      url: '/login',
      templateUrl: 'templates/auth/login.html',
      access : 'public'
    })
    .state('signup',{
      url: '/signup',
      templateUrl: 'templates/auth/signup.html',
      access : 'public'
    })
    .state('forgot',{
      url: '/forgot',
      templateUrl: 'templates/auth/forgot.html',
      access : 'public'
    })
    .state('password_reset',{
      url: '/forgot/password_reset?token',
      templateUrl: 'templates/auth/password_reset.html',
      access : 'public'
    })
    .state('email_verification_submit',{
      url: '/email_verification',
      templateUrl: 'templates/auth/email_verification.html',
      access : 'private'
    })
    .state('email_verification_verify',{
      url: '/email_verification/verify?token',
      templateUrl: 'templates/auth/email_verification.html',
      access : 'public'
    })
    .state('email_success',{
      url: '/forgot/email_success',
      templateUrl: 'templates/auth/email_success.html',
      access : 'public'
    })

    //---- PROFILE -----

    .state('profile',{
      url: '/profile',
      templateUrl: 'templates/profile/view.html',
      access : 'private'
    })
    .state('profile_edit',{
      url: '/profile/edit',
      templateUrl: 'templates/profile/edit.html',
      access : 'private'
    })
    .state('password_edit',{
      url: '/profile/edit/password',
      templateUrl: 'templates/profile/password.html',
      access : 'private'
    })
    .state('payment_edit',{
      url: '/profile/edit/payment',
      templateUrl: 'templates/profile/payment.html',
      access : 'private'
    })

    //----- USER -----

    .state('user_list',{
      url: '/users',
      templateUrl: 'templates/user/list.html',
      access : 'private'
    })
    .state('user',{
      url: '/users/:id',
      templateUrl: 'templates/user/view.html',
      access : 'private'
    })

    //---- MESSAGES -----

    .state('inbox',{
      url: '/inbox',
      templateUrl: 'templates/messages/list.html',
      access : 'private'
    })
    .state('conversation',{
      url: '/conversation/:id',
      templateUrl: 'templates/messages/view.html',
      access : 'private'
    })

    //---- SERVICES ----
   
    .state('services',{
      url: '/services',
      templateUrl: 'templates/service/list.html',
      access : 'public'
    })
    .state('service',{
      url: '/services/:id',
      templateUrl: 'templates/service/view.html',
      access : 'public'
    })
    .state('service_purchase',{
      url: '/services/:id/purchase',
      templateUrl: 'templates/service/purchase.html',
      access : 'private'
    })

    //---- CALL ----

    .state('call',{
      url: '/call/:id',
      templateUrl: 'templates/stream/rtc_test.html',
      access : 'private'
    })

    //---- PRIVACY ----
    .state('privacy',{
      url: '/privacy',
      templateUrl: 'templates/privacy.html',
      access : 'public'
    });

});

MentorMatch.run(function ($rootScope,$state,API,UserService,payOS) {
    
    $rootScope.$on('$stateChangeStart',
          function(event, toState, toParams, fromState, fromParams, options){ 

            if (toState.access === 'private'){
              
              if (!UserService.isUser()) event.preventDefault();
               
              

              API.isLogged(function(response){
                console.log(response);
                if (!UserService.isUser()){
                  
                  API.getProfile(function(response){

                    UserService.setUser(response.data);
                    //check if email is verified
                    API.isEmailVerified(function(response){
                      console.log(UserService.getUser());
                      if (!response.data.verified) $state.go('email_verification_submit');
                      else $state.go(toState);

                    },function(response){
                        // error
                    });

                  },function(response){
                     $state.go('login');
                  });
                }else{
                    //check if email is verified
            
                    API.isEmailVerified(function(response){
                      if (!response.data.verified)
                        $state.go('email_verification_submit');
                    },function(response){
                        // error
                    });
                }

              },function(response){
                UserService.setUser(null);
                $state.go('login');
              });

            }

          });

});

