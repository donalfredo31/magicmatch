# RecruiterMatch 

## Dependencies

* node v5, installed and configured via [nvm](https://github.com/creationix/nvm)
* [mongodb](https://www.mongodb.org/)

## Running the app

Ensure you're using node v5:
```
nvm use 5
```

Ensure MongoDB is running by running the following command in another terminal:
```
mongod
```

First install all the dependencies:

```
npm install
```

To run the server in dev mode (Watches for SASS changes and enables LiveReload):

```
npm run dev
```

Otherwise, to run a production instance:
```
NODE_ENV=production npm start
```