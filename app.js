

var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose'),
  passport = require('passport');

var path = require('path');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});
var app = express();



var http = require( "http" ).createServer(app);
var notificationHelper = require('./app/helpers/notification.js');
notificationHelper.init(http);

require('./config/express')(app, passport, mongoose, config);
require('./config/auth')(passport);


http.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});